3D Slicer 4.4

License and original binaries at:
http://slicer.org/

The only differences between the original tar.gz and this deb package are:

-wrapper script: /usr/local/bin/slicer
-binaries installed at: /opt/Slicer-VERSION-linux-amd64/

